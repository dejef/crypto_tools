// Jeffrey De La Mare
//

extern crate num; 
extern crate rand;

use num::{Integer, NumCast, PrimInt, Zero};
use simple_math::rand::Rng;
use std::fmt::{Debug, Display};
use std::str::FromStr;
use std::cmp::PartialEq;
use std::ops::{Sub, Div, Mul, Rem};


/// Creates an addition table. Given 5, shows (row * col) % 5
#[allow(unused)]
pub fn addition_table<T>(n: T) 
    where T: PartialEq + Copy + Integer + Rem + PrimInt + Display
{
    let mut finite_field = true;
    for i in num::range(T::zero(), n) {
        for j in num::range(T::zero(), n) {
            let val = (i + j) % n;
            if i != T::zero() || j != T::zero() && val == T::zero() {
                finite_field = false;
            }
            print!("{}", val);
        }
        println!();
    }
    if !finite_field { println!("Not a finite field"); }
}


/// Find le primes via brute force. It does print each prime.
#[allow(unused)]
pub fn brute_primes(n: usize) -> usize {
    let mut primes: Vec<u64> = vec![];
    match n { 
        0 => println!("Try again with a postive number"),
        1 => println!("1 is not prime"),
        2 => println!("2 is a prime number"),
        _ => ()
    }   
    for i in 2..=n {
        let mut prime: bool = true;
        for j in 2..i {
            if i % j == 0 { 
                prime = false;
            }
        }
        if prime {
            primes.push((i as u64).clone());
        }
    }   

    if !primes.is_empty() {
        println!("{:?}", primes);
    }

    primes.len()
}


/// Extended Euclidean Algorithm for finding the coefficients of Bezout's 
/// identity as well as the gcd.
/// 
/// # Examples
/// 
/// ```
/// use util::math;
/// let mut a: i64 = 1398; let mut b: i64 = 324;
/// let tup = math::extended_gcd(a, b);
/// 
/// assert_eq!((-19, 82, 6, -233, 54), tup);
/// ```
#[allow(unused)]
pub fn extended_gcd<T>(a: T, b: T) -> (T, T, T, T, T)
    where T: PartialEq + Copy + Integer + Mul + Div + Sub
{
    // http://www.math.cmu.edu/~bkell/21110-2010s/extended-euclidean.html
    // https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
    // iterative implementation
    let mut s:T = T::zero(); let mut old_s: T = T::one();
    let mut t:T = T::one(); let mut old_t: T = T::zero();
    let mut r:T = b; let mut old_r: T = a;

    while r != T::zero() {
        let quotient = old_r / r;

        let prov = r;
        r = old_r - quotient * prov;
        old_r = prov;

        let prov = s;
        s = old_s - quotient * prov;
        old_s = prov;

        let prov = t;
        t = old_t - quotient * prov;
        old_t = prov;
    }
    // (Bezout coefficient old_s & old_t, gcd, quotients by gcd t & s)
    (old_s, old_t, old_r, t, s)
}


/// Euclidean Algorithm for finding the greatest common denominator of 
/// two integers.
///
/// # Examples
///
/// ```
/// use util::math;
/// let mut a: i64 = 90; let mut b: i64 = 12;
/// let gcd = math::gcd(a, b);
///
/// assert_eq!(gcd, 6);
/// ```
#[allow(unused)]
pub fn gcd<T>(a: T, b: T) -> T 
    where T: Copy + PartialEq + Integer + NumCast + Zero 
{
    if a == T::zero() { 
        return b;
    }
    gcd(b % a, a) 
}


/// Gaussian eliminination 
/// This function requires specification of the desired type. A call would
/// look like. `gaussian_elim::<u32>(n)`
///
#[allow(unused)]
pub fn gaussian_elim<T>(n: usize, matrix: &mut Vec<Vec<T>>) 
    where T: Copy + FromStr + Integer + NumCast + Debug
{
    // potentially error out if the matrix is of incorrect size
    for i in 0..n {
        for j in i+1..n {
            for k in i..n+1 {
                matrix[j][k] = matrix[j][k] - matrix[i][k] * matrix[j][i] / matrix[i][i];
            }
        }
    }
}

/// Sieve of Eratosthenes
#[allow(unused)]
pub fn soe(n: usize) -> usize {
    let mut sieve: Vec<bool> = vec![true;n];
    sieve[0] = false;
    let b_cond: u64 = n as u64;
    if n > 7 {
        let b_cond: u64 = (n as f64).sqrt().floor() as u64;
    }
    for i in 2..b_cond {
        // Rust really wants these to be u64
        let mut curr: usize = i as usize * i as usize;
        while curr < n {
            sieve[curr] = false;
            curr += i as usize;
        }
    }

    let mut primes: Vec<usize> = vec![];
    for i in 0..sieve.len()-1 {
        if sieve[i] {
            primes.push(i);
        }
    }
    
    primes.len()
}

pub fn fermats_l_t(n: usize, k: usize) {
    for _ in 0..k {
        let a = rand::thread_rng().gen_range(3, n - 2);
        if gcd(a, n) != 1 {
            println!("composite");
            return;
        }
    }
    println!("probably prime");
}


/// Input #1: n > 3, an odd integer to be tested for primality;
/// Input #2: k, a parameter that determines the accuracy of the test
/// Output: composite if n is composite, otherwise probably prime
#[allow(unused)]
pub fn miller_rabin(n: u32, k: u32) {
    // write n − 1 as 2r·d with d odd by factoring powers of 2 from n − 1
    let mut d = n - 1;
    let mut r = 0;
    while d % 2 == 0 {
        d = d / 2;
        r += 1;
    }
    'witness: for _ in 0..k {
        let a = rand::thread_rng().gen_range(3, n - 2);
        let mut x = a.pow(d) % n;
        if x == 1 || x == n - 1{
            continue;
        }
        for _ in 0..r - 1 {
            x = x.pow(2) % n;
            if x == n-1 {
                continue 'witness;
            }
        }
        println!("composite");
    }
    println!("probably prime");
}

