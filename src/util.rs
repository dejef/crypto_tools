extern crate base64;
extern crate hex;

use std::fs::File;
use std::io::Read;

#[allow(unused)]
pub fn base64_to_hex(s: String) -> String {
    let s = vec_u8_to_char(hex::decode(s).expect("base64 decode failed"));
    let mut s = s.into_iter().collect();
    s = base64::encode(&s);

    s
}


#[allow(unused)]
pub fn vec_char_to_u8(vec: Vec<char>) -> Vec<u8> {
    let mut v: Vec<u8> = vec![0;vec.len()];
    for i in 0..vec.len() {
        v[i] = vec[i] as u8;
    }

    v
}


#[allow(unused)]
pub fn vec_u8_to_char(vec: Vec<u8>) -> Vec<char> {
    let mut v: Vec<char> = vec!['x';vec.len()];
    for i in 0..vec.len() {
        v[i] = vec[i] as char;
    }

    v
}


#[allow(unused)]
pub fn char_freq(vec: Vec<char>) -> Vec<(char, u64)> {
    let mut v: Vec<(char, u64)> = vec![];
    //could probably clean this up
    v.push(('A', 0)); v.push(('B', 0)); v.push(('C', 0));
    v.push(('D', 0)); v.push(('E', 0)); v.push(('F', 0));
    v.push(('G', 0)); v.push(('H', 0)); v.push(('I', 0));
    v.push(('J', 0)); v.push(('K', 0)); v.push(('L', 0));
    v.push(('M', 0)); v.push(('N', 0)); v.push(('O', 0));
    v.push(('P', 0)); v.push(('Q', 0)); v.push(('R', 0));
    v.push(('S', 0)); v.push(('T', 0)); v.push(('U', 0));
    v.push(('V', 0)); v.push(('W', 0)); v.push(('X', 0));
    v.push(('Y', 0)); v.push(('Z', 0));
    for i in 0..vec.len() - 1 as usize {
        match v.binary_search_by_key(&vec[i], |&(a,_)| a) {
            Ok(x) => v[x].1 += 1,
            _ => panic!("Not a valid character")
        }
    }

    v
}


// pass by reference and mutate vs new vec
#[allow(unused)]
pub fn filter_english_chars(chrs: Vec<char>) -> Vec<char>{
    let mut v: Vec<char> = vec![];
    for chr in chrs {
        if (chr.is_ascii() && chr.is_ascii_alphanumeric()) || chr == ' ' {
            v.push(chr.to_lowercase().to_string().chars().next().unwrap());
        }
    }

    v
}

// this function should probably trim whitespace from the edges, since it values whitespace.
#[allow(unused)]
pub fn is_english(text_block: &Vec<char>) -> (bool, f32) {
    let freq_arr: [(char, f32);27] = // an array of English letters and their frequency
        [(32 as char, 0.15000), (97 as char, 0.08167), (98 as char, 0.01492),
         (99 as char, 0.02782), (100 as char, 0.04323), (101 as char, 0.12702),
         (102 as char, 0.02228), (103 as char, 0.02015), (104 as char, 0.05924),
         (105 as char, 0.06094), (106 as char, 0.00153), (107 as char, 0.00772),
         (108 as char, 0.04025), (109 as char, 0.02406), (110 as char, 0.06949),
         (111 as char, 0.07607), (112 as char, 0.01929), (113 as char, 0.00095),
         (114 as char, 0.05987), (115 as char, 0.06327), (116 as char, 0.09056),
         (117 as char, 0.02718), (118 as char, 0.00978), (119 as char, 0.02360),
         (120 as char, 0.00150), (121 as char, 0.01974), (122 as char, 0.00074)];

    let total_num_chars = text_block.len();
    let mut num_alphabetic = 0; // number of seen characters that are English letters
    let mut score: f32 = 0.0;
    for chr in text_block {
        if chr.is_ascii_alphabetic() || chr.is_ascii_whitespace() {
            num_alphabetic += 1;
            match freq_arr.binary_search_by_key(&((chr.to_lowercase().to_string().as_bytes())[0]), |&(a,_)| a as u8) {
                Ok(x) => score += freq_arr[x].1,
                _ => panic!("shouldn't reach this")
            }
        }
    }

    //filter all of the strings that have unreasonable number of non-English characters
    if 0.62 < num_alphabetic as f32 / total_num_chars as f32 {
        return (true, score);
    }

    (false, score)
}


//util function
#[allow(unused)]
pub fn file_to_vec(file_name: String) -> Vec<char> {
    let mut file_handle = File::open(&file_name).expect("file not found");
    let mut text = String::new();
    file_handle.read_to_string(&mut text)
                          .expect("something went wrong reading the file");

    text.chars().collect()
}


#[allow(unused)]
pub fn pad(len: usize, unpadded_str: &mut Vec<u8>) -> Result<(), &'static str> {
    match unpadded_str.len() < len {
        true => {
            let i = len - unpadded_str.len();
    
            for _ in 0..i {
                unpadded_str.push(4); 
            }   
        },  
        false => return Err("padding length cannot be less than total length.")
    }   

    Ok(())
}
