extern crate num; 
extern crate bit_vec;

pub mod simple_math;
pub mod simple_crypt;
pub mod util;
