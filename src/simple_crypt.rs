extern crate hex;
extern crate bit_vec;

use bit_vec::BitVec;
use std::str::from_utf8;
// sbxor = single byte XOR
// rkxor = rolling key XOR

#[allow(unused)]
pub fn xor_strings(s1: String, s2: String) -> String {
    let a = s1.into_bytes();
    let b = s2.into_bytes();
    let soln: Vec<u8> = a.into_iter().zip(b).map(|x| x.0 ^ x.1).collect();
    let soln: String = String::from_utf8(soln).unwrap();

    soln
}

#[allow(unused)]
pub fn xor_hex_strings(s1: String, s2: String) -> String {
    let a = hex::decode(s1).expect("hex decode failed");
    let b = hex::decode(s2).expect("hex decode failed");
    let soln: Vec<u8> = a.into_iter().zip(b).map(|x| x.0 ^ x.1).collect();
    let soln = hex::encode(soln);

    soln
}

#[allow(unused)]
pub fn caesar_cipher(mut vec: Vec<u8>) -> Vec<String> {
    let mut shifts:Vec<String> = vec!["".to_string();26];
    for i in 0..vec.len() {
        vec[i as usize].make_ascii_lowercase();
    }
    for i in 0..26 {
        for j in 0..vec.len() {
            if (vec[j] + i).is_ascii_alphabetic() {
                vec[j] += i;
            } else {
                vec[j] = (vec[j] + i) - 26;
                println!("{:?}", vec[j]);
            }
        }
        shifts.push(from_utf8(&vec).unwrap().to_string());
    }

    shifts
}


// rkxor a phrase with a key
#[allow(unused)]
pub fn rkxor_ascii_key(ciphertext: &mut Vec<u8>, mut key: Vec<u8>) {
    let mut key_posn = 0;
    let mut cipher_posn = 0;
    while ciphertext.len() > cipher_posn {
        if key.len() <= key_posn {
            key_posn = 0;
        }
        let encoding_char = &mut key[key_posn] as *mut u8;
        let r = &mut ciphertext[cipher_posn] as *mut u8;

        unsafe {
            *r = *r ^ (*encoding_char as u8);
        }

        cipher_posn += 1;
        key_posn += 1;
    }
}


/// Splits a slice of text up into keysize portions
#[allow(unused)]
pub fn split_blocks_keysize(text: &str, keysize: usize) -> Vec<String> {
    let mut l_win_edge: usize = 0;
    let mut r_win_edge = keysize;
    let mut blocks: Vec<String> = vec![];
    while r_win_edge + keysize < text.len() {
        blocks.push(text.to_string().get(l_win_edge..r_win_edge).unwrap().to_string());
        l_win_edge += keysize;
        r_win_edge += keysize;
    }

    blocks
}


/// Uses a window of varying size to scroll over the
/// strings. The size of the window is known as keysize
/// whose value changes in the outer loop. Each string
/// within the window is passed to the hamming function.
/// returns a vector containing keys that are potential
/// candidates.
#[allow(unused)]
pub fn find_keysizes(text: &str) -> Vec<(f64, usize)> {
    let mut result: Vec<(f64, usize)> = vec![]; // (distance, keysize)
    for keysize in 2..41 {
        let mut l_win_edge = 0; // window that scrolls through the available characters
        let mut r_win_edge = keysize;
        let mut dist: f64 = 0.0;
        let mut dist_list: Vec<f64> = vec![];
        while r_win_edge < text.len() - keysize {
            let string1 = &text[l_win_edge..r_win_edge];
            l_win_edge += keysize;
            r_win_edge += keysize;
            let string2 = &text[l_win_edge..r_win_edge];
            l_win_edge += keysize;
            r_win_edge += keysize;
            dist = hamming(string1.to_string(), string2.to_string());
            dist = dist as f64 / keysize as f64;
            dist_list.push(dist);
        }
        let mut avg: f64 = 0.0;
        let len: f64 = dist_list.len() as f64;
        for dist in dist_list { // i think other dist is shadowed in the forloop
            avg += dist;
        }
        let avg = avg/len;
        result.push((avg, keysize));
    }

    result
}


/// Transposes blocks provided in a vector of strings. There will be
/// keysize number of transposed blocks, returned in another vector
/// of strings. Does NOT operate in place.
#[allow(unused)]
pub fn transpose(blocks: Vec<String>, keysize: usize) -> Vec<String> {
    let mut transposed_blocks: Vec<String> = vec![];
    for _ in 0..keysize {
        transposed_blocks.push(String::new());
    }
    for block in blocks {
        let mut i = 0;
        let block_bytes: &[u8] = block.as_bytes();
        for byte in block_bytes {
            transposed_blocks[i].push(char::from(*byte));
            i += 1;
        }
    }

    transposed_blocks
}


#[allow(unused)]
pub fn hamming(string1: String, string2: String) -> f64 {
    let mut distance = 0;
    let bv1 = BitVec::from_bytes(string1.as_bytes());
    let bv2 = BitVec::from_bytes(string2.as_bytes());
    let bv_tuple = bv1.iter().zip(bv2.iter());
    for pair in bv_tuple {
        if pair.0 != pair.1 {
            distance += 1;
        }
    }

    distance as f64
}


// it's probably bad to use nested vectors
// any way you cut it, gonna need 256 vectors
#[allow(unused)]
pub fn decode_sbxor_ascii(vec: Vec<u8>) -> Vec<Vec<char>> {
    let mut vv: Vec<Vec<char>> = vec![];
    for i in 0..255 {
        let v = vec.clone();
        let v: Vec<u8> = v.into_iter().map(|x| x ^ i).collect();
        let chrs_after_xor: Vec<char> = v.into_iter().map(|x| x as char).collect();
        let mut soln: Vec<char> = vec![]; // holy crap this is too many vectors
        for chr in chrs_after_xor {
            if (chr.is_ascii() && chr.is_ascii_alphanumeric()) || chr == ' ' {
                soln.push(chr.to_lowercase().to_string().chars().next().unwrap());
            }
        }

        vv.push(soln);
    }

    vv
}


#[allow(unused)]
pub fn sbxor_ascii_key(v: Vec<u8>, encoding_byte: u8) -> Vec<u8> {
    let v: Vec<u8> = v.into_iter().map(|x| x ^ encoding_byte).collect();

    v
}
